FROM trixter1394/evo_base

COPY ./ /app

WORKDIR /app

ARG ENVIRONMENT=development
ENV RUNTIME_ENVIRONMENT ${ENVIRONMENT}
ENV GITLAB_TOKEN ''

EXPOSE 3000

RUN bundle install

CMD ["/bin/sh", "-c", "/app/bin/rails server --environment $RUNTIME_ENVIRONMENT"]