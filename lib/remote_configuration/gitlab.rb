# frozen_string_literal: true

require 'httparty'
require 'json'
require 'base64'
require 'yaml'

module RemoteConfiguration
  class Gitlab
    include HTTParty

    base_uri 'https://gitlab.com/api/v4/projects'

    PRIVATE_TOKEN = ENV['GITLAB_TOKEN']

    def initialize
      @options = {
        headers: { 'Private-Token' => PRIVATE_TOKEN }
      }
    end

    def config(file_name)
      return {} if ENV['RAILS_ENV'] == 'test'

      path = "/10544311/repository/files/#{file_name}.yml?ref=master"
      response = self.class.get(path, @options)

      return {} if response['content'].nil?

      decoded_file = Base64.decode64(response['content'])
      config_yaml = YAML.safe_load(decoded_file)
      combine_config(config_yaml)
    end

    private

    def combine_config(config_yaml)
      config = config_yaml['default']

      if config_yaml.key? ENV['RAILS_ENV']
        config_yaml[ENV['RAILS_ENV']].each do |key, value|
          config[key] = value
        end
      end

      config
    end
  end
end
