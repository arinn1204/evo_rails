# frozen_string_literal: true
require 'httparty'
require_relative '../model/omdb'

module Omdb
  module Api
    class Search
      include HTTParty

      def initialize(proxy_url: 'http://www.omdbapi.com/', api_key: '')
        @api_key = api_key
        self.class.base_uri(proxy_url)

      end

      def get_by_id(id, api_key: @api_key, **extra_options)
        options = {
          query: {
            apikey: api_key,
            r: 'json',
            i: id
          }
        }
        extra_options.each { |k, v| options[:query][k] = v }
        send_request options
      end

      def get_by_title(title, api_key: @api_key, **extra_options)
        options = {
          query: {
            apikey: api_key,
            r: 'json',
            t: title.tr(' ', '+')
          }
        }
        extra_options.each { |k, v| options[:query][k] = v }
        send_request options
      end

      private

      def send_request(options)
        ::Omdb::Model::Omdb.new(self.class.get('/', options))
      end

      attr_reader :api_key
    end
  end
end
