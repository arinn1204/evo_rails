# frozen_string_literal: true

module Omdb
  module Model
    class Omdb
      def initialize(response)
        @response = response
      end

      def title
        @response['Title']
      end

      def actors
        to_list(@response, 'Actors')
      end

      def genres
        to_list(@response, 'Genre')
      end

      def imdb_id
        @response['imdbID']
      end

      private

      def to_list(response, field)
        response[field].split(',').map(&:strip)
      end

      attr_reader :response
    end
  end
end
