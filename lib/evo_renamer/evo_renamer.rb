require_relative 'renamer/renamer'
require_relative 'formatter/formatter'
require_relative 'helpers/string'

module EvoRenamer
  include Renamer
  include Formatter
end