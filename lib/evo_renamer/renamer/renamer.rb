# frozen_string_literal: true

require_relative '../formatter/formatter'

module EvoRenamer
  module Renamer
    class Renamer
      def initialize()
        @reject = %w[
          REMASTERED BRRIP BOKUTOX
          YIFY IMAX HDTV VTV tvu.org.ru
          TVU ORG RU DD5 1-BLITZCRIEG
          BLURAY TCRIP HQ AC3 DC
          DIRECTORS CUT GAZ YTS -SVA
        ]
      end

      def get_title(file_name)
        file_name_regex = '(^.*?)(?:' \
        '(?=[ (]\d{3,4})|' \
        '(?=[ (]\d{1,2}x\d{1,2})|' \
        '(?=[ (]s\d{1,2}e\d{1,2})' \
        ')'

        prepare_file_name(file_name) \
          .match(file_name_regex)[0] \
          .titleize \
          .chomp(' ')
      end

      def get_year(file_name)
        year_regex = '\(?[^\[]\d{4}\)?[^\]p]'

        match = prepare_file_name(file_name).match(year_regex)
        return nil if match.nil?

        match[0].gsub(/[\(\)]/, '').to_i
      end

      def get_resolution(file_name)
        movie = FFMPEG::Movie.new(file_name)
        resolution(movie.resolution)
      end

      def get_codec(file_name)
        movie = FFMPEG::Movie.new(file_name)
        movie.video_codec
      end

      private

      def resolution(incoming_resolution)
        resolution_hash = {
          '1920x1080': '1080p',
          '1280x720': '720p',
          '2048x1080': '2k',
          '2560x1440': '1440p',
          '3840x2160': '4k',
          '7680x4320': '8k'
        }

        resolution_hash[incoming_resolution.to_sym]
      end

      def prepare_file_name(file_name)
        file_name.gsub(/(\s+|\.+)/, ' ').downcase
      end
    end
  end
end
