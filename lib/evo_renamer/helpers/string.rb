class String
  def titleize
    self.split.map(&:capitalize).join(' ')
  end
end