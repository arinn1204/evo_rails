FactoryBot.define do
  factory :torrent do
    app_id { SecureRandom.uuid }
    file_name { Faker::Lorem.word }
    magnet { URI 'http://www.google.com' }
    torrent_id { SecureRandom.uuid }
  end
end