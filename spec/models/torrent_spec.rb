require 'rails_helper'

RSpec.describe Torrent, type: :model do
  #ensuring the model has the three fields
  it { should validate_presence_of(:app_id) }
  it { should validate_presence_of(:file_name) }
  it { should validate_presence_of(:magnet) }
  it { should validate_presence_of(:torrent_id) }
end
