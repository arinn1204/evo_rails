require 'swagger_helper'

describe 'Healthcheck API' do
  path '/healthcheck' do
    get 'Checks current health' do
      response '204', 'No data' do
        run_test!
      end
    end
  end

  path '/torrents' do
    get 'Returns all previous torrents' do
      response '200', 'Ok' do
        run_test!
      end
    end

    post 'Creates new torrent' do
      tags 'Torrents'
      consumes 'application/json'

      parameter name: :torrent, in: :body, schema: {
        type: :object,
        properties: {
          file_name: { type: :string },
          app_id: { type: :string, format: :uuid },
          torrent_id: { type: :string, format: :uuid },
          magnet: { type: :string }
        },

        required: %w(file_name app_id torrent_id magnet)
      }

      response '201', 'torrent created' do
        let(:torrent) do
          {  file_name: 'foobar',
             app_id: SecureRandom.uuid,
             torrent_id: SecureRandom.uuid,
             magnet: 'magnet:fjdaklsjfl' }
        end
        run_test!
      end

      response '400', 'bad request' do
        let(:torrent) { { file_name: 'foobar' } }
        run_test!
      end
    end

  end

end