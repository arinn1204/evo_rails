# frozen_string_literal: true

require 'stringio'
require 'streamio-ffmpeg'

module EvoRenamer
  module Support
    def generate_video(video, resolution: '1080p', codec: 'libx264')
      silent_run do
        create_video(video, resolution, codec).run
      end
    end

    def create_video(video, inc_resolution, codec)
      FFMPEG::Transcoder.new(
        '',
        video,
        { video_codec: codec, frame_rate: 25, aspect: '4:3' },
        input: '/dev/zero',
        input_options: input_options(inc_resolution)
      )
    end

    def silent_run
      previous_stdout = $stdout
      $stdout = StringIO.new
      FFMPEG.logger = Logger.new($stdout)

      yield
    ensure
      $stdout = previous_stdout
    end

    protected

    def resolution
      {
        '1280x720': '720p',
        '1920x1080': '1080p',
        '2048x1080': '2k',
        '2560x1440': '1440p',
        '3840x2160': '4k',
        '7680x4320': '8k'
      }
    end

    private

    def input_options(inc_resolution)
      res_map = resolution.invert
      {
        t: '1',
        f: 'rawvideo',
        s: res_map[inc_resolution].to_s
      }
    end

  end
end
