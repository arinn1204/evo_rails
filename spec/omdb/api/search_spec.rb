# frozen_string_literal: true

require_relative '../../../lib/omdb/omdb'
require_relative '../../spec_helper'
require 'service_mock'
require 'yaml'

describe 'Omdb API' do
  let!(:wiremock_config) do
    YAML.load_file("#{__dir__}/../../../config/wiremock.yml")
  end

  let(:wiremock) do
    ServiceMock::Server.new(
      "standalone-#{wiremock_config['version']}"
    )
  end

  let(:stub_creator) { ServiceMock::StubCreator.new(wiremock) }
  let(:api_key) { 'foobar' }
  let(:proxy_url) { wiremock_config['host'] }
  let(:proxy_port) { wiremock_config['port'] }
  let(:search_api) do
    ::Omdb::Api::Search.new(
      proxy_url: "#{proxy_url}:#{proxy_port}"
    )
  end

  before do
    wiremock.start do |s|
      s.remote_host = proxy_url
      s.port = proxy_port
      s.verbose = true
    end
    stub_creator.create_stubs_with('the_avengers.yml')
  end

  after do
    wiremock.reset_all
  end

  context 'search by ID' do
    let(:id) { 'tt0848228' }

    before do
      @results = search_api.get_by_id(id, api_key: api_key)
    end

    it 'should return movie title for The Avengers' do
      expect(@results.title).to eq('The Avengers')
    end

    it 'should return list of actors' do
      actors = @results.actors

      expect(actors.class).to eq([].class)
      expect(actors.count).to eq(4)
      expect(actors).to eq [
                               'Robert Downey Jr.',
                               'Chris Evans',
                               'Mark Ruffalo',
                               'Chris Hemsworth'
                           ]
    end

    it 'should return list of genres' do
      genres = @results.genres

      expect(genres.class).to eq([].class)
      expect(genres.count).to eq(3)
      expect(genres).to eq %w[Action Adventure Sci-Fi]
    end
  end

  context 'search by Title' do
    let(:title) { 'The Avengers' }

    before do
      @results = search_api.get_by_title(title, api_key: api_key)
    end

    it 'should return imdb id of movie' do
      expect(@results.imdb_id).to eq('tt0848228')
    end
  end
end
