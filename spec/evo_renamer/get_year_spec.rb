require 'evo_renamer/evo_renamer'

require_relative '../spec_helper'

describe 'Evo Renamer' do
  let(:renamer) { EvoRenamer::Renamer::Renamer.new }

  describe 'should parse the year from a file name' do

    def self.test_format(expected, file)
      it "should get the year: '#{expected}' from the file '#{file}'" do
        expect(renamer.get_year(file)).to eq(expected)
      end
    end

    context 'using different tv formats' do

      test_format(nil, 'Sons  of anarchy 2x01.[1080p].mkv')
      test_format(2009, 'Archer (2009) 105.H264.mkv')
      test_format(2009, 'Supernatural.2009.720p.x264.mkv')

    end

    context 'using different movie formats' do

      test_format(2012, 'The.Avengers.2012.x264.[1080p].avi')
      test_format(2012, 'The.Avengers.(2012) x264.[1080p].avi')

    end


  end

end