require 'evo_renamer/evo_renamer'

require_relative '../spec_helper'

describe 'Evo Renamer' do
  let(:renamer) { EvoRenamer::Renamer::Renamer.new }

  describe 'getting title of a generic video' do

    def self.test_format(expected, file)
      it "should get the title: '#{expected}' from the file'#{file}'" do
        expect(renamer.get_title(file)).to eq(expected)
      end
    end

    context 'using different tv formats' do

      test_format('Sons Of Anarchy','Sons  of anarchy 2x01.[1080p].mkv')
      test_format('Archer', 'Archer (2009) 105.H264.mkv')
      test_format('Archer', 'Archer   (2009) 105.H264.mkv')
      test_format('Arrow', 'Arrow.S07E08..720p.HDTV.x264-SVA.mkv')
      test_format('Arrow', 'Arrow....S07E08.720p.HDTV.x264-SVA.mkv')
      test_format('The Walking Dead', 'The.Walking.Dead.S5e3.1080p.mp4')
      test_format('Supernatural', 'Supernatural.2009.720p.x264.mkv')

    end

    context 'different movie formats' do

      test_format('The Avengers', 'The.Avengers.2012.x264.[1080p].avi')

    end

  end
end