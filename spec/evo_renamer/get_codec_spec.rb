# frozen_string_literal: true

require 'evo_renamer/evo_renamer'
require_relative '../spec_helper'

describe 'Evo Renamer' do
  include EvoRenamer::Support
  let(:renamer) { EvoRenamer::Renamer::Renamer.new }
  let(:video_dir) { 'spec/videos' }
  let(:video_h264) { "#{video_dir}/Game.Of.Thrones.Codec.libx264.mkv" }
  let(:video_h265) { "#{video_dir}/Game.Of.Thrones.Codec.libx265.mkv"}

  before do
    Dir.mkdir video_dir unless File.directory? video_dir
    generate_video(video_h264)
    generate_video(video_h265, codec: 'libx265')
  end

  after do
    FileUtils.rm_rf video_dir
  end

  it 'should retrieve libx264/h264 codec from file' do
    silent_run do
      expect(renamer.get_codec(video_h264)).to eq('h264')
    end
  end

  it 'should retrieve libx265/hevc codec from file' do
    silent_run do
      expect(renamer.get_codec(video_h265)).to eq('hevc')
    end
  end

end
