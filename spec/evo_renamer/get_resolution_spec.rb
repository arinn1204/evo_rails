# frozen_string_literal: true

require 'evo_renamer/evo_renamer'
require_relative '../spec_helper'

include EvoRenamer::Support

describe 'Evo Renamer' do
  let(:renamer) { EvoRenamer::Renamer::Renamer.new }
  let(:video_dir) { 'spec/videos' }
  let(:video) { "#{video_dir}/Game.Of.Thrones.1080p.mkv" }
  let(:video_720) { "#{video_dir}/Game.Of.Thrones.720p.mkv" }

  before do
    Dir.mkdir video_dir unless File.directory? video_dir
    generate_video(video)
    generate_video(video_720, resolution: '720p')
  end

  after do
    FileUtils.rm_rf video_dir
  end

  it 'should retrieve codec from 1080p file' do
    silent_run do
      expect(renamer.get_resolution(video)).to eq('1080p')
    end
  end

  it 'should retrieve resolution from 720p file' do
    silent_run do
      expect(renamer.get_resolution(video_720)).to eq('720p')
    end
  end

end
