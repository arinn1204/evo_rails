# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Torrents API', type: :request do
  let!(:torrents) { create_list(:torrent, 10) }
  let(:torrent_id) { torrents.first.id }

  describe 'GET /torrents' do
    before { get '/torrents' }

    it 'returns torrents' do
      expect(json).not_to be_empty
      expect(json.size).to eq 10
    end

    it 'returns successful response' do
      expect(response).to have_http_status 200
    end
  end

  describe 'POST /torrents' do
    context 'when the request is valid' do
      before do
        post '/torrents', params: {
          file_name: 'foobar',
          app_id: SecureRandom.uuid,
          torrent_id: SecureRandom.uuid,
          magnet: 'https://www.google.com'
        }
      end

      it 'creates a torrent' do
        expect(json['file_name']).to eq 'foobar'
      end

      it 'returns status code 201' do
        expect(response).to have_http_status 201
      end
    end

    context 'when the request is invalid' do
      before do
        post '/torrents',
             params: {
               file_name: 'foobar',
               app_id: SecureRandom.uuid,
               torrent_id: SecureRandom.uuid
             }
      end

      it 'returns status code 400' do
        expect(response).to have_http_status 400
      end

      context 'with no file_name entered' do
        before do
          post '/torrents',
               params: {
                 app_id: SecureRandom.uuid,
                 torrent_id: SecureRandom.uuid,
                 magnet: 'https://www.google.com'
               }
        end

        it 'returns validation failure message' do
          expect(response.body)
            .to match(/file_name cannot be nil/)
        end
      end

      context 'with no app_id entered' do
        before do
          post '/torrents',
               params: {
                 file_name: 'foobar',
                 torrent_id: SecureRandom.uuid,
                 magnet: 'https://www.google.com'
               }
        end

        it 'returns validation failure message' do
          expect(response.body)
            .to match(/app_id cannot be nil/)
        end
      end

      context 'with no torrent_id entered' do
        before do
          post '/torrents',
               params: {
                 file_name: 'foobar',
                 app_id: SecureRandom.uuid,
                 magnet: 'https://www.google.com'
               }
        end

        it 'returns validation failure message' do
          expect(response.body)
            .to match(/torrent_id cannot be nil/)
        end
      end

      context 'with no magnet entered' do
        before do
          post '/torrents',
               params: {
                 file_name: 'foobar',
                 app_id: SecureRandom.uuid,
                 torrent_id: SecureRandom.uuid
               }
        end

        it 'returns validation failure message' do
          expect(response.body)
            .to match(/magnet cannot be nil/)
        end
      end
    end
  end

  describe 'PUT /torrents' do
    before { put "/torrents/#{torrent_id}" }

    it 'returns status code 405' do
      expect(response).to have_http_status 405
    end
  end

  describe 'DELETE /torrents' do
    before { delete "/torrents/#{torrent_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status 204
    end
  end
end
