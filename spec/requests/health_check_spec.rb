require 'rails_helper'

RSpec.describe 'Health Check API', type: :request do
  context 'GET /healthcheck' do
    before { get '/healthcheck' }

    it 'returns successful' do
      expect(response).to have_http_status(204)
    end

  end
end