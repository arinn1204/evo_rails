class CreateTorrents < ActiveRecord::Migration[5.2]
  def change
    create_table :torrents do |t|
      t.string :app_id
      t.string :file_name
      t.string :magnet

      t.timestamps
    end
  end
end
