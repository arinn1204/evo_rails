class AddIndexToTorrents < ActiveRecord::Migration[5.2]
  def change
    add_index :torrents, :torrent_id, unique: true
  end
end
