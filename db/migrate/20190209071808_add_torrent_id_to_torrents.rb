class AddTorrentIdToTorrents < ActiveRecord::Migration[5.2]
  def change
    add_column :torrents, :torrent_id, :string
  end
end
