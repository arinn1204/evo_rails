FROM ruby:2.4.5-stretch

RUN apt-get update && \
    apt-get install -y ffmpeg && \
    apt-get clean -y && apt-get autoremove -y && \
    gem install bundler:2.0.1 pg:'< 2.0' rails:5.2.2 streamio-ffmpeg:3.0.2