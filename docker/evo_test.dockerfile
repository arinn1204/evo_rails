FROM trixter1394/evo_base

RUN apt-get update && \
    apt-get install -y default-jre && \
    apt-get clean -y && apt-get autoremove -y && \
    gem install service_mock database_cleaner factory_bot_rails faker shoulda-matchers rswag rspec-rails
