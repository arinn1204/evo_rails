require_relative '../../lib/remote_configuration/gitlab'
require 'pry'
require 'pry-byebug'

module Evo
  class Application < Rails::Application
    config.before_configuration do
      gitlab = RemoteConfiguration::Gitlab.new
      remote_config = gitlab.config('evo')

      remote_config.each do |key, value|
        ENV[key] = value
      end
    end
  end
end