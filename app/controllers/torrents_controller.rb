# frozen_string_literal: true

class TorrentsController < ApplicationController
  before_action :validate_request, only: %i[create]

  def index
    torrents = Torrent.all
    json_response(torrents)
  end

  def show
    torrent = set_torrent
    json_response(torrent)
  end

  def destroy
    set_torrent.destroy
    head :no_content
  end

  def update
    head :method_not_allowed
  end

  def create
    torrent = Torrent.create! torrent_params
    json_response(torrent, :created)
  end


  private

  def torrent_params
    params.permit(:magnet, :torrent_id, :app_id, :file_name)
  end

  def validate_request
    return json_response({error: 'magnet cannot be nil'},
                         400) if params[:magnet].nil?

    return json_response({error: 'torrent_id cannot be nil'},
                         400) if params[:torrent_id].nil?

    return json_response({error: 'app_id cannot be nil'},
                         400) if params[:app_id].nil?

    return json_response({error: 'file_name cannot be nil'},
                         400) if params[:file_name].nil?

    true
  end

  def set_torrent
    Torrent.find(params[:id])
  end

end
