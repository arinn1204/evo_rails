class Torrent < ApplicationRecord
  validates_presence_of :app_id, :file_name, :magnet, :torrent_id
end

